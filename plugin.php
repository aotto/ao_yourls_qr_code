<?php
/*
Plugin Name: QR Code for Short URLS
Plugin URI: http://code.otto-hanika.de/ao_yourls_qr_code
Description: Add .qr to shorturls to display QR Code
Version: 1.0
Author: Andreas Otto
Author URI: http://code.otto-hanika.de/
License: GPL 3 or later
*/

// Size of QR Code
// To overwrite the default value define this constant in config.php.
if(!defined('AO_YOURLS_QR_CODE_SIZE')) {
	define( 'AO_YOURLS_QR_CODE_SIZE', 5 );
}

// Error correction level of QR Code
// L = 0, M = 1, Q = 2, H = 3
// To overwrite the default value define this constant in config.php.
if(!defined('AO_YOURLS_QR_ECLEVEL')) {
	define( 'AO_YOURLS_QR_ECLEVEL', 1 );
}

// QR Code Logo
// To add a logo to your qr codes define this constant in config.php
// and add your logo to the folder resources.
// define( 'AO_YOURLS_QR_CODE_LOGO', 'logo.png' );

// Kick in if the loader does not recognize a valid pattern
yourls_add_action( 'loader_failed', 'ao_yourls_qr_code' );

/**
 * @param $request
 */
function ao_yourls_qr_code( $request ) {
		// Get authorized charset in keywords and make a regexp pattern
	$pattern = yourls_make_regexp_pattern( yourls_get_shorturl_charset() );

		// Shorturl is like bleh.qr ?
	if( preg_match( "@^([$pattern]+)\.qr\.([0-9]{1,2})?/?$@", $request[0], $matches ) || preg_match( "@^([$pattern]+)\.qr?/?$@", $request[0], $matches ) ) {
			// Does the shorturl exist?
		$keyword = yourls_sanitize_keyword( $matches[1] );
			// Is a size requested?
		if ($matches[2]) {
			$size = intval($matches[2]);
		} else {
			$size = AO_YOURLS_QR_CODE_SIZE;
		}
		if( yourls_is_shorturl( $keyword ) ) {
				// Show the QR code then!
			$filename = ao_yourls_qr_code_getFilename($keyword, AO_YOURLS_QR_ECLEVEL, $size);
			ao_yourls_qr_code_make_file($filename, $keyword, AO_YOURLS_QR_ECLEVEL, $size);
		}
	}
}

/**
 * Get the filename of the qr code.
 *
 * @param	string	$keyword
 * @param	int		$eclevel
 * @param	int		$size
 * @return	string
 */
function ao_yourls_qr_code_getFilename($keyword, $eclevel = AO_YOURLS_QR_ECLEVEL, $size = AO_YOURLS_QR_CODE_SIZE) {
	return md5($keyword.$eclevel.$size).'.png';
}

/**
 * Get the name of the directory where the qr codes are stored.
 *
 * @param	string	$filename
 * @return	string
 */
function ao_yourls_qr_code_getDirectory($filename) {
	return dirname(__FILE__) . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . substr($filename, 0, 1) . DIRECTORY_SEPARATOR;
}

/**
 * Check if file and directory exists.
 *
 * @param	string	$filename
 * @param	string	$keyword
 * @param	int		$eclevel
 * @param	int		$size
 * @return	void
 */
function ao_yourls_qr_code_make_file($filename, $keyword, $eclevel = AO_YOURLS_QR_ECLEVEL, $size = AO_YOURLS_QR_CODE_SIZE) {
	$directory = ao_yourls_qr_code_getDirectory($filename);
	$file = $directory . $filename;
	if(file_exists($file)) {
		ao_yourls_qr_code_send_file($file);
	}else{
		if(!is_dir($directory)) {
			mkdir($directory, 0777, TRUE);
		}
		include_once('phpqrcode/qrlib.php');
		QRcode::png(YOURLS_SITE.'/'.$keyword, $file, $eclevel, $size);
		if(defined('AO_YOURLS_QR_CODE_LOGO')) {
			ao_yourls_qr_code_addLogo($file);
		}
		ao_yourls_qr_code_send_file($file);
	}
}

/**
 * Send file to browser/client.
 *
 * @param string $file
 * @return void
 */
function ao_yourls_qr_code_send_file($file) {
	$fp = fopen($file, 'rb');
	header('Content-Type: image/png');
	header('Content-Length: ' . filesize($file));
	fpassthru($fp);
	exit;
}

function ao_yourls_qr_code_addLogo($file) {
	$logo = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . AO_YOURLS_QR_CODE_LOGO;
	$logoInfo = getimagesize($logo);
	$fileInfo = getimagesize($file);

	$src1 = imagecreatefrompng($file);
	$src2 = imagecreatefrompng($logo);

	$dest = imagecreatetruecolor($fileInfo[0], $fileInfo[1]);

	imagealphablending($src1, TRUE);

	imagealphablending($src2, TRUE);

	imagealphablending($dest, TRUE);

	$bgcol=imagecolorallocate($dest, 255, 255, 255);
	imagefill($dest, 0, 0, $bgcol);
	imagecopy($dest, $src1, 0, 0, 0, 0, $fileInfo[0], $fileInfo[1]); //have to play with these numbers for it to work for you, etc.
	imagecopy($dest, $src2, ($fileInfo[0]/2)-($logoInfo[0]/2), ($fileInfo[1]/2)-($logoInfo[1]/2), 0, 0, $logoInfo[0], $logoInfo[1]); //have to play with these numbers for it to work for you, etc.

	imagepng($dest, $file, 9);

	imagedestroy($dest);
	imagedestroy($src1);
	imagedestroy($src2);
}