This plugin allows to create qr codes of short URLs by adding ".qr" to the short URL.
It is also possible to request a larger qr code by adding ".qr.<size>", e. g. "qr.10" to the short URL.
The generated qr codes will be stored in the folder "temp/<first char of filename>/<filename>" inside the plugin's folder.

The creation of the plugin was inspired by http://code.google.com/p/yourls/wiki/PluginQRCodeShortURL.
This plugin uses the PHP library from http://phpqrcode.sourceforge.net/ to create qr codes.